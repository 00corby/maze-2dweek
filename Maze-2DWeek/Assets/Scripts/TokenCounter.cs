using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TokenCounter : MonoBehaviour
{
    Text countText;

    // Got script for this and Tokens from https://sharpcoderblog.com/blog/unity-2d-coin-pickup

    // Start is called before the first frame update
    void Start()
    {
        countText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //Set the current number of coins to display
        if (countText.text != Tokens.totalCoins.ToString())
        {
            countText.text = "Tokens: " + Tokens.totalCoins.ToString();
        }
    }
}